/*! \file 4011-DS1821test.c
 * 
 *  \brief Read DS1821 digital thermometer and display result
 * 
 *  \author jjmcd
 *  \date December 22, 2012, 3:39 PM
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/lcd.h"
#include "../../include/delay.h"
#include "../../include/DS1821.h"

// Configuration fuses
/* ***NOTE*** The DS1821 library will not work properly at other
 * clock speeds unless recompiled with adjustments to delays            */
_FOSC (XT_PLL16);                       /* 7.3728 rock * 16 = 118MHz    */
_FWDT (WDT_OFF);                        /* Watchdog timer               */
_FBORPOR (PWRT_16 & BORV27 & MCLR_EN);  /* Brownout 2.7, powerup 16ms   */
_FGS (GWRP_OFF & CODE_PROT_OFF);        /* No code protection           */


/*! Read the DS1821 digital thermometer */
/*! Reads the Dallas Semiconductor DS1821 Programmable Digital Thermostat
 *  and Thermometer and displays the result in Fahrenheit and Centigrade
 */
int main(int argc, char** argv)
{
    int temperature;
    double fTemp;
    char szWork[32];

    /* Initialize the timer used for the DS1821 */
    initialize1821();
    /* We are going to flash a LED for each update */
    _LATD2 = 1;
    _TRISD2 = 0;
    /* Initialize and clear the LCD */
    LCDinit();
    LCDclear();

    while(1)
    {
        /* Read the temperature from the 1821 (returns centigrade) */
        temperature = readTemp1821();
        /* Put the temperatures into a string for display */
        _LATD2 = 0;
        sprintf(szWork,"%2d\337C    %3d\337F        ",
                temperature,           /* Centigrade */
                9*temperature/5+32);   /* Fahrenheit */
        /* Display the result on the top line */
        LCDhome();
        LCDputs(szWork);
        _LATD2 = 1;

        LCDline2();

        fTemp = readTempHR1821();

        sprintf(szWork,"%5.2f\337C %6.2f\337F   ",fTemp,
                9.0*fTemp/5.0+32.0);
        LCDputs(szWork);

        /* Wait a bit before doing it again */
        //Delay(Delay_1S_Cnt);
        //Delay(Delay_1S_Cnt);
    }

    return (EXIT_SUCCESS);
}

